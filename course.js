const http = require('http');



let port = 3000

const server = http.createServer(function(request, response){
	//add route
	if (request.url == "/" && request.method ==='GET'){
		//
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to booking system")

	}

		if (request.url == "/profile" && request.method ==='GET'){
		//
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to your profile")
	}
	if (request.url == "/courses" && request.method ==='GET'){
		//
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Here's our courses available");

	}



	else if (request.url == "/addCourse" && request.method === 'POST'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Add course to our resources.")
	}

	else if (request.url == "/updateCourse" && request.method === 'PUT'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Update course to our resources.");

	}
	else if (request.url == "/archiveCourse" && request.method === 'PUT'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Archive course to our resources.");

	}
});

server.listen(port)
console.log(`Server now accessible at localhost: ${port}`)
